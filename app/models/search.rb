require_dependency "homefinder/search"

#Search as a domain (form object pattern) enables form_for, validations (and, future/yagni, saved searches)
class Search
  # below extends/include to enable table-less model. Rails 4 provides a bit easier way, but this app is Rails 3.
  extend  ActiveModel::Naming
  extend  ActiveModel::Translation
  include ActiveModel::Validations
  include ActiveModel::Conversion
  #doesn't appear to be a way to get attributes/attributes= from ActiveRecord.  If needed, may have to be home-implemented.

  #adapter to simplify results being handed to front-end, and specify what comes out of to_json
  class Results
    #original_search is intended for pagination, though 'searchResultsUrl' from webservice call may
    #also be usable for that
    attr_reader :listings, :total_matched, :total_pages, :current_page, :original_search

    def initialize(listings = [], total_matched = 0,
                   total_pages = 0, current_page = 0, original_search)
      @listings = listings
      @total_matched = total_matched
      @total_pages = total_pages
      @current_page = current_page
      @original_search = original_search
    end

    #rails' to_json appears to dump instance vars instead of methods, so re-implement here to get methods.  involve jbuilder or the
    # like if this gets any more complicated.
    def to_json
      {
          total_matched: total_matched,
          total_pages: total_pages,
          current_page: current_page,
          listings: listings,
          original_search: original_search
      }
    end
  end


  attr_accessor :zip, #Hf 'area' fxnality adds time to implementation of this. only handling zip for this. this doesn't then map to service call exactly..
                :bed,
                :bath,
                :page #,:dogs_allowed

  validates :zip, :presence => true, :format => {:with => /\A[0-9]{5}\Z/,
                                                 :message => "must be 5 digit format."}
  validates :bed, :numericality => true, :allow_blank => true  #TODO integer only? assuming not for now.  also, check if this allows negatives
  validates :bath, :numericality => true, :allow_blank => true
  validates :page, :format => /\A[0-9]{10}\Z/, :allow_nil => true

  # support for table-less model
  def persisted?
    false
  end

  def initialize(attrs = {})
    attrs.each { |k,v|
      public_send "#{k}=", v  #note that this will choke ungracefully on extra attrs not in object
    }
  end

  #performs search and returns Search::Results obj
  def results
    Homefinder::Search.new.results self
  end

end