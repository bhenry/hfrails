(function() {
    //fxns to handle ajax-based search.  home-rolling leaves things unhandled (back button, refresh, etc.), but
    //introducing Ember, Backbone, Angular, etc. is not in scope.

    var clearErrors = function() {
        $('#errors').empty();
    };

    var clearResultsList = function() {
        $('#results').empty();
    };

    var updatePagination = function(data) {
        var current = data['current_page'],
            total = data['total_pages'],
            paginationElem = $('#pagination');

        //TODO pagination - requires links/buttons that resubmit original_search (to $('#new_search')[0].action or get by making this an
        // erb template), with a new page #, and also being listening to ajax events and updating listings.
        //currently handle search as a POST, so would likely want to use same mechanism in pagination request.  form
        // may not match req params, if user changes them and then clicks pagination link, so would likely want to handle that as well.

        //this should probably be templated in the html page and just swap in vars here, and make prev/next conditionally visible or not
        //if(current > 1) paginationElem.append(' Prev ');
        paginationElem.html('Page ' + current + ' of ' + total);  //append is no good - search 2x gives you 2x printing of this.
        //if(current < total) paginationElem.append(' Next ');
    };

    var updateResultsList = function(data) {
        var listingsArray = data['listings'],
            listingContainer,
            listingTemplate = $('#listing_template'),
            resultsContainer = $('#results');

        //console.log(listingsArray);
        _.each(listingsArray, function(listing) {
            //console.dir(listing);
            listingContainer = $.clone(listingTemplate[0]);

            //almost none of these nested field checks is nullsafe currently.  TODO fix.
            $('.results_type', listingContainer).html(listing.type);
            $('.results_isrental', listingContainer).html(listing.isRental ? 'Yes' : 'No');
            $('.results_price', listingContainer).html(listing.price).formatCurrency();
            if(listing.primaryPhoto) {
                $('.results_primary_photo', listingContainer)
                    .html('<img class="thumb" src="' + listing.primaryPhoto.url + '"/>');
            }
            $('.results_neighborhood', listingContainer).html(listing.address.neighborhood);
            $('.results_description', listingContainer).html(listing.description);
            $('.results_bed', listingContainer).html(listing.bed);
            $('.results_bath_total', listingContainer).html(listing.bath ? listing.bath.total : "?");
            $('.results_agent_name', listingContainer).html(listing.agent.name);

            //make visible/add
            $(listingContainer).appendTo(resultsContainer);
            listingContainer.style.display = 'block';
        });
    };


    $(document).ready(function () {
        var searchForm = $('#new_search');

        searchForm.on("ajax:success", function (e, data, status, xhr) {
            clearErrors();
            clearResultsList();
            updatePagination(data);
            updateResultsList(data);
        });

        //shortcutting on this, for time's sake.  in real app, this would be more reasonably robust, refactored out to fxn, etc.
        searchForm.on("ajax:error", function (e, xhr, status, error) {
            var k,
                errorJson,
                errorString = "",
                errorsContainer = $('#errors');

            if (xhr.status != 500) {
                errorJson = xhr.responseJSON['errors'];
                for (k in errorJson) {
                    errorString += k + " " + errorJson[k] + "<br />";  //for now, just dumping array
                    //console.log(errorString);
                }
                errorsContainer.html("Error: " + errorString);
            } else {
                errorsContainer.html("Unspecified Server Error. Our deepest apologies.");
            }
        });
    });

}());