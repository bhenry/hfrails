#main controller for this exercise.  supplies index page, to serve HTML frontend, and the search fxality, as a JSON-trafficking
#ajax call.
class ListingsController < ApplicationController

  # renders initial search form
  def index
    @search = Search.new
  end


  #performs search and renders results
  def search
    @search = Search.new(params['search'])

    if @search.valid?
      @results = @search.results

      render json: @results.to_json
    else
      # as rendered by to_json, search obj always has 'errors' hash, even if it's an empty one (as in success case), so
      # throw that down, to make 'errors' available for display
      render json: @search.to_json, status: :bad_request
    end
  end

end
