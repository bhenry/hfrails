module Homefinder
  class Search
    include HTTParty

    base_uri Rails.application.config.homefinder_svc_url
    default_params :apikey => Rails.application.config.homefinder_svc_apikey
    format :json #need to trigger resp parsing into json

    MODE_SPOOF = "spoof" #for experimenting, testing off-network
    MODE_LIVE = "live" #the real deal - hit HF servers

    def mode=(mode)
      @mode = mode
    end

    #spoof vs. live mode
    def mode
      @mode ||= Rails.application.config.homefinder_svc_mode
    end


    #takes local app's Search domain and returns local app's Search::Results object (which is a wrapper around JSON, at
    # present, for simplicity), populated with results from searching via HF web service.
    def results(search_obj = Search.new)
      if MODE_SPOOF == mode
        output = JSON.parse(File.read("fake_service_data/search.json")) #array of string-based hashes
      else
        param_hash = extract_params search_obj
        logger.debug  param_hash

        #currently, will expection in response to failure HTTP status codes, but ignoring "status" field in
        # a returned json body for now
        #below invokes HTTParty-provided get()
        output = self.class.get('/listingServices/search', query: param_hash)
      end

      logger.debug("output from service call: #{output}")
      data = output['data']
      listings = data ? data['listings'] : []

      md = data ? data['meta'] : {}
      if md.present?
        total_matched = md['totalMatched']
        total_pages = md['totalPages']
        current_page = md['currentPage'] + 1 #currentPage is coming back 0-based
      else
        total_matched = total_pages = current_page = 0
      end
      ::Search::Results.new(listings, total_matched, total_pages, current_page, search_obj)
    end


    private

    def logger
      Rails.logger
    end

    #pull HF service params from attributes of Search model,
    def extract_params(search_obj)
      #no way to get the attributes of the non-AR domain at present, so specify each conversion/param.
      # key is outgoing param, val is app-local attrib
      {
          :area => search_obj.zip, #note this is assuming what is valid for local form is valid for webservice, for simplicity.  otherwise, could deal with conversion fxns.
          :bed => search_obj.bed,
          :bath => search_obj.bath,
          :page => search_obj.page
      }
    end

  end
end

